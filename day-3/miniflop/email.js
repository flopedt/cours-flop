function draw_table(tutors) {
  let line = d3
    .select('table')
    .selectAll('.entries')
    .data(tutors)
    .enter()
    .append("tr");
  
  line
    .append("td")
    .text(function(t) { return t.first_name; });
  line
    .append("td")
    .text(function(t) { return t.last_name; });
  line
    .append("td")
    .text(function(t) { return t.username; });
  line
    .append("td")
    .text(function(t) { return t.email; });
}


$.ajax({
  type: "GET",
  dataType: 'text',
  url: url_a_changer,
  async: true,
  contentType: "text/csv",
  success: function(msg, ts, req) {
    let tutors = d3.csvParse(msg);
    draw_table(tutors);
  },
  error: function(msg) {
            console.log("error");
  }
});


