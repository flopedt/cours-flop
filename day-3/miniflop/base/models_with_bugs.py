# -*- coding: utf-8 -*-

# This file is part of the MiniFlOp project.
# Copyright (c) 2019
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from enum import Enum

# <editor-fold desc="GROUPS">
# ------------
# -- GROUPS --
# ------------

class Group(models.Model):
    name = models.CharField(max_length=4)
    parent_group = models.ForeignKey('self',
                                     blank=True,
                                     null=True,
                                     related_name="children_group",
                                     on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def ancestor_groups(self):
        """
        :return: the set of all Group containing self (self not included)
        """
        all = set()

        if self.parent_group:
            all.add(self.parent_group)
            all|=self.parent_group.ancestor_groups()

        return all


# </editor-fold desc="GROUPS">

# <editor-fold desc="TIMING">
# ------------
# -- TIMING --
# ------------

# will be used only for constants
# TO BE CLEANED at the end (fields and ForeignKeys)
class Day(Enum):
    MONDAY = "m"
    TUESDAY = "tu"
    WEDNESDAY = "w"
    THURSDAY = "th"
    FRIDAY = "f"
    SATURDAY = "sa"
    SUNDAY = "su"

# </editor-fold>

# <editor-fold desc="ROOMS">
# -----------
# -- ROOMS --
# -----------


class RoomType(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Room(models.Model):
    name = models.CharField(max_length=2)
    room_type = models.OneToOneField(RoomType,
                                   blank=True,
                                   related_name="members")

# </editor-fold>

# <editor-fold desc="COURSES">
# -------------
# -- COURSES --
# -------------


class Module(models.Model):
    nom = models.ForeignKey(max_length=100, null=True)
    abbrev = models.CharField(max_length=10, verbose_name='Intitulé abbrégé')
    head = models.ForeignKey('Tutor',
                             null=True,
                             default=None,
                             blank=True,
                             on_delete=models.CASCADE)

    def __str__(self):
        return self.abbrev

    class Meta:
        ordering = ['abbrev', ]



class ScheduledCourse(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    day = models.CharField(max_length=2, default=Day.MONDAY,
                           choices=[(d, d.value) for d in Day])
    # in minutes from 12AM
    start_time = models.PositiveSmallIntegerField()
    room = models.ForeignKey('room', blank=True, null=True, on_delete=models.CASCADE)


# </editor-fold desc="COURSES">




# <editor-fold desc="DISPLAY">
# -------------
# -- DISPLAY --
# -------------


class ModuleDisplay(models.Model):
    module = models.OneToOneField('Module', related_name='display')
    color_bg = models.CharField(max_length=20, default="red")
    color_txt = models.CharField(max_length=20, default="black")

    def __str__(self):
        return f"{self.module} -> BG: {self.color_bg} ; TXT: {self.color_txt}"


class GroupDisplay(models.Model):
    group = models.OneToOneField('Group',
                                 related_name='display',
                                 on_delete=models.CASCADE)
    button_height = models.PositiveIntegerField(null=True, default=None)


    def __str__(self):
        return f"{self.group} -> BH: {self.button_height} ; " + \
               f"BTXT: {self.button_txt}"


# </editor-fold desc="DISPLAY">
