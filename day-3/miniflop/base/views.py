from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.staticfiles.views import serve

# Create your views here.
def hello_world(request):
    return HttpResponse("Salut !")


def display_year(request, year):
    return render(request,
                  'base/edt.html',
                  {'yyyy': year})


def display(request, year, week):
    if year is None or week is None:
        year = 2019
        week = 6
    else:
        year = int(year)
        week = int(week)
        
    return render(request,
                  'base/edt.html',
                  {'yyyy': year,
                   'ww': week})


def fetch_courses(request, year, week):

    if year not in [2019, 2020]:
        return serve(request,
                     'base/data.csv')

    return serve(request,
                 'base/data' + str(year) + '.csv')
