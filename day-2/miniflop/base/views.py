from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.staticfiles.views import serve

# Create your views here.
def hello_world(request):
    return HttpResponse("Salut !")


def display_year(request, year):
    return render(request,
                  'base/edt.html',
                  {'yyyy': year})


def display(request, year):
    if year is None:
        year = 2019
    else:
        year = int(year)
        
    return render(request,
                  'base/edt.html',
                  {'yyyy': year})


def fetch_courses(request, year):

    if year not in [2018, 2019]:
        return serve(request,
                     'base/data.csv')

    return serve(request,
                 'base/data' + str(year) + '.csv')
