import json

from django.shortcuts import render

from django.http import HttpResponse, JsonResponse

from .utils import Game

# Create your views here.
def hello(request):
    return HttpResponse("Salut !")


def play(request):
    return render(request, 'main_app/morpion.html', {})

def save_game(request):
    game = Game(json.loads(request.POST.get('game',{})))
    game.print_game()
    
    return HttpResponse(status=204)    

def load_game(request):
    game = Game()
    
    return JsonResponse(game.export_to_json(None),
                        safe=False)
