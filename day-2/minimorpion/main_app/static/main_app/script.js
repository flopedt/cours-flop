/***************/
/* static grid */
/***************/
/*
   ______ (0,0)
  |
  v                         x
  o-----+-----+-----+------->
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     <----->
  |    len_square
  |
  v
y

*/


// ---------------------------
// Grid Layout
// ---------------------------
var len_square = 100 ;

d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", len_square)
    .attr("y1", 0)
    .attr("x2", len_square)
    .attr("y2", 3*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 2*len_square)
    .attr("y1", 0)
    .attr("x2", 2*len_square)
    .attr("y2", 3*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 0)
    .attr("y1", len_square)
    .attr("x2", 3*len_square)
    .attr("y2", len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 0)
    .attr("y1", 2*len_square)
    .attr("x2", 3*len_square)
    .attr("y2", 2*len_square);

// ---------------------------
// Draw the pawns
// ---------------------------

var radius = 40 ;
var defaultColor = "#fafafa";
var game = new Object();
var players = new Map();
var turns   = new Array();

// returns the circle center x coordinate
function move_x(m) {
    return m.x * len_square + len_square/2 ;
}
// returns the circle center y coordinate
function move_y(m) {
    return m.y * len_square + len_square/2 ;
}
// returns the color of the player that played this move
function move_color(m) {
  var playerName = m.p;
  if ( players.has(playerName) ) {
    return players.get(playerName).color;
  } else {
    return defaultColor;
  }
}
// propose another player for this move
function move_click(m) {
    var actual_player = turns.indexOf(m.p);
    m.p = turns[ (actual_player+1) % turns.length ];
    display_move() ;
    console.log( m.p );
}

function display_move() {
  var moves = 
  d3.select("svg#grille")
    .selectAll("circle")
    .data(game.cells);

  moves
    .enter()
    .append("circle")
    .attr("cx", move_x)
    .attr("cy", move_y)
    .attr("r", radius)
    .attr("fill", move_color)
    .on("click",move_click);

  moves
    .merge(moves)
    .attr("fill", move_color);

}

function new_game() {
  for (var i=0; i<game.cells.length; i++) {
    game.cells[i].p = "n";
  }
  display_move();
}


console.log("On est pret pour une partie !");

// send the current state of the game to the server
function send_game() {

    var post_args = {}
    var sent_game = {}
    sent_game['players'] = Array.from(players.keys()) ;
    sent_game['cells'] = game.cells ;
    post_args['game'] = JSON.stringify(sent_game) ;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: post_args,
        url: url_save,
        success: function(msg) {
            console.log("Looks like we succeeded.")
        },
        error: function(msg) {
            console.log("Looks like we failed.")
        }
    });
}

function on_game_receive(g) {
    game = new Object() ;
    game.cells = g.cells ;
    game.players = g.players ;
    
    // create a Map to store players informations : [name]=>{name,color,...}
    //    and an Array to represent the game turns ["n","j1","j2"]
    players = new Map();
    turns   = new Array();
    for (var i=0; i<game.players.length; i++ ) {
        players.set( game.players[i].name , game.players[i] );
        turns[i] = game.players[i].name;
    }
    turns[i] = "n";
    
    display_move() ;
}


function get_game() {
    $.ajax({
        type: "GET",
        dataType: 'text',
        contentType: "text/json",
        async: true,
        // url hardcoded for now, we will do better soon
        url: "/morpion/load",
        success: function(msg) {
	    var parsed = JSON.parse(msg);
            console.log(parsed);
            on_game_receive(parsed);
        },
        error: function(msg) {
            console.log("Couldn't fetch the game, sorry.");
        }
    });
}

get_game();
