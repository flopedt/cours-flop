from django.urls import path

from main_app import views

app_name="main_app"

urlpatterns = [
    path('hello/', views.hello, name="hello"),
    path('play/', views.play, name="play"),
    path('save/', views.save_game, name="save"),
    path('load/', views.load_game, name="load")
]
