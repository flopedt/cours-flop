/***************/
/* static grid */
/***************/
/*
   ______ (0,0)
  |
  v                         x
  o-----+-----+-----+------->
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     <----->
  |    len_square
  |
  v
y

*/


// ---------------------------
// Grid Layout
// ---------------------------
let len_square = 100 ;

d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", len_square)
    .attr("y1", 0)
    .attr("x2", len_square)
    .attr("y2", 3*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 2*len_square)
    .attr("y1", 0)
    .attr("x2", 2*len_square)
    .attr("y2", 3*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 0)
    .attr("y1", len_square)
    .attr("x2", 3*len_square)
    .attr("y2", len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 0)
    .attr("y1", 2*len_square)
    .attr("x2", 3*len_square)
    .attr("y2", 2*len_square);

// ---------------------------
// Draw the pawns
// ---------------------------

let radius = 40 ;
let defaultColor = "#fafafa";
let game = new Object();
game.players = [{"name":"FP","color":"#00FFFF"},
                {"name":"RB","color":"#FFFF00"}
               ];
game.cells = [{"x":0,"y":0, "p":"FP"},
              {"x":0,"y":1, "p":"n"},
              {"x":0,"y":2, "p":"n"},
              {"x":1,"y":0, "p":"FP"},
              {"x":1,"y":1, "p":"RB"},
              {"x":1,"y":2, "p":"n"},
              {"x":2,"y":0, "p":"RB"},
              {"x":2,"y":1, "p":"n"},
              {"x":2,"y":2, "p":"RB"}
             ];

// create a Map to store players informations : [name]=>{name,color,...}
//    and an Array to represent the game turns ["n","j1","j2"]
let players = new Map();
let turns   = new Array();
let i ;
for (i=0; i<game.players.length; i++ ) {
  players.set( game.players[i].name , game.players[i] );
  turns[i] = game.players[i].name;
}
turns[i] = "n";
// returns the circle center x coordinate
function move_x(m) {
    return m.x * len_square + len_square/2 ;
}
// returns the circle center y coordinate
function move_y(m) {
    return m.y * len_square + len_square/2 ;
}
// returns the color of the player that played this move
function move_color(m) {
  let playerName = m.p;
  if ( players.has(playerName) ) {
    return players.get(playerName).color;
  } else {
    return defaultColor;
  }
}
// propose another player for this move
function move_click(m) {
    let actual_player = turns.indexOf(m.p);
    m.p = turns[ (actual_player+1) % turns.length ];
    display_move() ;
    console.log( m.p );
}

function display_move() {
  let moves = 
  d3.select("svg#grille")
    .selectAll("circle")
    .data(game.cells);

  let new_moves = moves
    .enter()
    .append("circle")
    .attr("cx", move_x)
    .attr("cy", move_y)
    .attr("r", radius)
    .on("click",move_click);

  new_moves
    .merge(moves)
    .attr("fill", move_color);

}
display_move();

function new_game() {
  for (let i=0; i<game.cells.length; i++) {
    game.cells[i].p = "n";
  }
  display_move();
}


console.log("On est pret pour une partie !");

// send the current state of the game to the server
function send_game() {

    let post_args = {} ;
    let sent_game = {} ;
    sent_game['players'] = [] ;
    sent_game['cells'] = [] ;
    post_args['game'] = JSON.stringify(sent_game) ;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: post_args,
        url: "where-shall-i-send-it",
        success: function(msg) {
            console.log("Looks like we succeeded.") ;
        },
        error: function(msg) {
            console.log("Looks like we failed.") ;
        }
    });
}

